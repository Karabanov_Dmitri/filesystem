import java.io.*;
import java.util.Scanner;

public class Searcher extends Thread {



    public static void main(String[] args)  {
        System.out.println("искомый текст");
        Scanner scanner=new Scanner(System.in);

        String text=scanner.next();
        System.out.println("путь к файлам");
        File folder = new File(scanner.next());
        processFilesFromFolder(folder, text);
    }


    public static void processFilesFromFolder(File folder, String text)
    {
        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries)
        {
            if (entry.isDirectory())
            {
                processFilesFromFolder(entry, text);
                continue;
            }
            fileTextSearcher(entry, text);
        }
    }

    private static void fileTextSearcher(File file, String text) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file));
             BufferedWriter writer = new BufferedWriter(new FileWriter("ResultOfSearch", true))) {
            String string;
            int count = 0;
            while ((string = reader.readLine()) != null) {
                count++;
                if (string.contains(text)) {
                    writer.write("Текст найден в файле " + file.getName() + " в " + count + " строке " +
                            ", который находится " + file.getParent() + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
